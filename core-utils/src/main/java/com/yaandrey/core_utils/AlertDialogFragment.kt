package com.yaandrey.core_utils

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class AlertDialogFragment : DialogFragment() {
    private val keyTitle = "keyTitle"
    private val keyMessage = "keyMessage"
    private var callBack: (FragmentActivity.() -> Unit)? = null

    companion object {
        fun instance(title: String, message: String): AlertDialogFragment {
            return AlertDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(keyTitle, title)
                    putString(keyMessage, message)
                }
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle(arguments?.getString(keyTitle))
            setMessage(arguments?.getString(keyMessage))
            setPositiveButton("OK") { dialog, _ ->
                dialog.dismiss()
            }
        }.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onDestroyView() {
        val dialog = dialog
        // handles https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && retainInstance) {
            dialog.setDismissMessage(null)
        }
        super.onDestroyView()
    }

    fun show(
        fragmentManager: FragmentManager,
        tag: String?,
        callBack: FragmentActivity.() -> Unit
    ) {
        this.callBack = callBack
        show(fragmentManager, tag)
    }
}