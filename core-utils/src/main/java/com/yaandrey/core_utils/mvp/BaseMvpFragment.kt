package com.yaandrey.core_utils.mvp

import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.yaandrey.core_utils.AlertDialogFragment
import moxy.MvpAppCompatFragment

abstract class BaseMvpFragment : MvpAppCompatFragment(), BaseMvpView {

    override fun showError(message: String) {
        Log.e("ERROR_LOG", "Error: $message")
        val view = view ?: return
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    }

    override fun showDialog(title: String, message: String) {
        AlertDialogFragment.instance(title, message).show(childFragmentManager, null)
    }
}