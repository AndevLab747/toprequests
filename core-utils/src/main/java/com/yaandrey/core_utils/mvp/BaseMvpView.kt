package com.yaandrey.core_utils.mvp

import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface BaseMvpView : MvpView {
    fun showError(message: String)
    fun showDialog(title: String, message: String)
}