package com.yaandrey.core_utils.mvp

import android.app.Application
import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
abstract class BaseMvpPresenter<VIEW : BaseMvpView> (
    application: Application
) : MvpPresenter<VIEW>(), CoroutineScope by MainScope() {
    val context: Context = application.applicationContext

    fun getString(res: Int, vararg args: Any): String = context.getString(res, *args)
}