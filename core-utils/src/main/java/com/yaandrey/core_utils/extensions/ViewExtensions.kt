package com.yaandrey.core_utils.extensions

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun Activity.hideKeyboard() {
    val view = currentFocus ?: View(this)
    (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.showKeyboard(view: View) {
    val view = currentFocus ?: View(this)
    (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
}

fun Fragment.hideKeyBoard() {
    val view = requireActivity().currentFocus ?: View(requireContext())
    (requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun EditText.afterFocusChanged(afterFocusChangeListener: (Boolean) -> Unit) {
    this.setOnFocusChangeListener { _, hasFocus -> afterFocusChangeListener.invoke(hasFocus) }
}

fun <F : Fragment> AppCompatActivity.getFragment(fragmentClass: Class<F>): F? {
    val fragment = this.supportFragmentManager.fragments.first() as Fragment

    fragment.childFragmentManager.fragments.forEach {
        if (fragmentClass.isAssignableFrom(it.javaClass)) {
            return it as F
        }
    }
    return null
}

fun <T> AppCompatActivity.observe(data: LiveData<T>, eventCallBack: (T) -> Unit) {
    data.observe(this, Observer { event ->
        event?.let {
            eventCallBack(event)
        }
    })
}

fun <T> Fragment.observe(data: LiveData<T>, eventCallBack: (T) -> Unit) {
    data.observe(this, Observer { event ->
        event?.let {
            eventCallBack(event)
        }
    })
}