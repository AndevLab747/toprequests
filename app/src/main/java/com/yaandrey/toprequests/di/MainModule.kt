package com.yaandrey.toprequests.di

import com.yaandrey.toprequests.ui.MainPresenter
import com.yaandrey.toprequests.ui.fragments.MainFragmentPresenter
import org.koin.dsl.module
import org.koin.experimental.builder.factory

val mainModule = module {
    factory<MainPresenter>()
    factory<MainFragmentPresenter>()
}