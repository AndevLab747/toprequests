package com.yaandrey.toprequests.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.core_usecase.model.SearchResultModel
import com.yaandrey.toprequests.R
import com.yaandrey.toprequests.ui.fragments.MainViewHolder

class MainAdapter : RecyclerView.Adapter<MainViewHolder>() {

    private val results = ArrayList<SearchResultModel>()

    fun show(results: List<SearchResultModel>) {
        this.results.clear()
        this.results.addAll(results)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder =
        MainViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_search_result, parent, false)
        )

    override fun getItemCount(): Int = results.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) = holder.bind(results[position])
}