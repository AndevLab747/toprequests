package com.yaandrey.toprequests.ui

import android.app.Application
import com.yaandrey.core_utils.mvp.BaseMvpPresenter
import moxy.InjectViewState

@InjectViewState
class MainPresenter (
    application: Application
) : BaseMvpPresenter<MainView>(application) {

}