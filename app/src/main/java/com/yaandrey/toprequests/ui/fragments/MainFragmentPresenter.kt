package com.yaandrey.toprequests.ui.fragments

import android.app.Application
import com.example.core_usecase.repository.db.DataBaseRepository
import com.example.core_usecase.repository.network.NetworkRepository
import com.yaandrey.core_utils.mvp.BaseMvpPresenter
import com.yaandrey.toprequests.API_KEY
import com.yaandrey.toprequests.PARAMETER_CX
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class MainFragmentPresenter(
    application: Application,
    private val db: DataBaseRepository,
    private val networkRepository: NetworkRepository
) : BaseMvpPresenter<MainFragmentView>(application) {


    fun searchResult(searchText: String) = launch {
        val result = withContext(Dispatchers.IO) { networkRepository.loadSearchResult(searchText) }
        db.insertResults(result)
        viewState.addResults(result)
    }

    fun getLastTopResults() = launch {
        val result =  withContext(Dispatchers.IO) { db.getLastTenSearchResult() } ?: return@launch
        viewState.addResults(result)
    }
}