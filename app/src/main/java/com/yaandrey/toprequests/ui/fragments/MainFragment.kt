package com.yaandrey.toprequests.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.core_usecase.model.SearchResultModel
import com.yaandrey.core_utils.extensions.hideKeyBoard
import org.koin.android.ext.android.get
import com.yaandrey.core_utils.mvp.BaseMvpFragment
import com.yaandrey.toprequests.R
import com.yaandrey.toprequests.adapter.MainAdapter
import kotlinx.android.synthetic.main.fragment_main.*
import moxy.ktx.moxyPresenter

class MainFragment : BaseMvpFragment(), MainFragmentView {
    companion object {
        fun newInstance() = MainFragment()
    }

    private val presenter by moxyPresenter { get<MainFragmentPresenter>() }
    private lateinit var mainAdapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickListeners()
        initRecyclerView()
        presenter.getLastTopResults()
    }

    private fun clickListeners() {
        button_search.setOnClickListener { initSearch() }
    }

    private fun initSearch() {
        val textSearch = search_edit_text.text.toString()
        if (textSearch.isEmpty()) {
            showError("Вы ничего не ввели")
        } else {
            hideKeyBoard()
            presenter.searchResult(textSearch)
        }
    }

    private fun initRecyclerView() {
        mainAdapter = MainAdapter()
        recycler_search_result.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mainAdapter
        }
    }

    override fun addResults(results: List<SearchResultModel>) {
        mainAdapter.show(results)
    }

}