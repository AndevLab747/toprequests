package com.yaandrey.toprequests.ui.fragments

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.core_usecase.model.SearchResultModel
import com.squareup.picasso.Picasso
import com.yaandrey.toprequests.R
import kotlinx.android.synthetic.main.item_search_result.view.*

class MainViewHolder(view: View): RecyclerView.ViewHolder(view) {
    private val title: AppCompatTextView = view.title
    private val url: AppCompatTextView = view.url_web_site
    private val description: AppCompatTextView = view.description

    fun bind(item: SearchResultModel) {
        title.text = item.title
        url.text = item.url
        description.text = item.description


    }
}