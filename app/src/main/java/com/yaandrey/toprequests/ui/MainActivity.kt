package com.yaandrey.toprequests.ui

import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import com.yaandrey.core_utils.AlertDialogFragment
import com.yaandrey.toprequests.R
import com.yaandrey.toprequests.ui.fragments.MainFragment
import kotlinx.android.synthetic.main.activity_main.*
import moxy.MvpAppCompatActivity

class MainActivity : MvpAppCompatActivity(),
    MainView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }

    override fun showError(message: String) {
        Log.e("ERROR_LOG", "Error: $message")
        Snackbar.make(container, message, Snackbar.LENGTH_LONG).show()
    }

    override fun showDialog(title: String, message: String) {
        AlertDialogFragment.instance(title, message).show(supportFragmentManager, null)
    }
}