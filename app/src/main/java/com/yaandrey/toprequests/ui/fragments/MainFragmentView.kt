package com.yaandrey.toprequests.ui.fragments

import com.example.core_usecase.model.SearchResultModel
import com.yaandrey.core_utils.mvp.BaseMvpView
import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface MainFragmentView : BaseMvpView {

    fun addResults(results: List<SearchResultModel>)
}