package com.yaandrey.toprequests

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
import com.yaandrey.core_network.di.networkModule
import com.yaandrey.core_storage.di.storageModule
import com.yaandrey.toprequests.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        @Volatile
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    mainModule,
                    storageModule,
                    networkModule
                )
            )
        }
        if (BuildConfig.DEBUG) {
            initMonitoring()
        }
    }

    private fun initMonitoring() {
        Stetho.initialize(
            Stetho.newInitializerBuilder(this)
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .build()
        )
    }
}