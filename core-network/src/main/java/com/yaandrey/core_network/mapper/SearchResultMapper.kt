package com.yaandrey.core_network.mapper

import com.example.core_usecase.model.SearchResultModel
import com.yaandrey.core_network.dto.SearchResultDto

object SearchResultMapper : Mapper<SearchResultModel>() {
    override fun map(from: SearchResultDto): List<SearchResultModel> {
        val arraySearchResult = ArrayList<SearchResultModel>()
        from.items.forEach {
            arraySearchResult.add(SearchResultModel(it.title, it.link, it.snippet))
        }
        return arraySearchResult
    }
}