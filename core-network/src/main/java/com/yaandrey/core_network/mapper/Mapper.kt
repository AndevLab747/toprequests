package com.yaandrey.core_network.mapper

import com.yaandrey.core_network.dto.SearchResultDto

abstract class Mapper<To> {
    abstract fun map (from: SearchResultDto): List<To>
}