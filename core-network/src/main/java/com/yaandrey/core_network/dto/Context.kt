package com.yaandrey.core_network.dto


import com.google.gson.annotations.SerializedName

data class Context(
    @SerializedName("title")
    val title: String
)