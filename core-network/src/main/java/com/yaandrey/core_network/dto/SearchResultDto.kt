package com.yaandrey.core_network.dto


import com.google.gson.annotations.SerializedName

data class SearchResultDto(
    @SerializedName("context")
    val context: Context,
    @SerializedName("items")
    val items: List<Item>,
    @SerializedName("kind")
    val kind: String,
    @SerializedName("queries")
    val queries: Queries,
    @SerializedName("searchInformation")
    val searchInformation: SearchInformation,
    @SerializedName("url")
    val url: Url
)