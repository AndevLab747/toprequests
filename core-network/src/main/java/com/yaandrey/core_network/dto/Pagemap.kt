package com.yaandrey.core_network.dto


import com.google.gson.annotations.SerializedName

data class Pagemap(
    @SerializedName("cse_image")
    val cseImage: List<CseImage>,
    @SerializedName("cse_thumbnail")
    val cseThumbnail: List<CseThumbnail>?,
    @SerializedName("hproduct")
    val hproduct: List<Hproduct>,
    @SerializedName("metatags")
    val metatags: List<Metatag>,
    @SerializedName("website")
    val website: List<Website>
)