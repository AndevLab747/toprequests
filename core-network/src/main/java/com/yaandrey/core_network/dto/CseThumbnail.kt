package com.yaandrey.core_network.dto


import com.google.gson.annotations.SerializedName

data class CseThumbnail(
    @SerializedName("height")
    val height: String,
    @SerializedName("src")
    val src: String,
    @SerializedName("width")
    val width: String
)