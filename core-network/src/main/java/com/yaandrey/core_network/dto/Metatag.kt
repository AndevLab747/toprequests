package com.yaandrey.core_network.dto


import com.google.gson.annotations.SerializedName

data class Metatag(
    @SerializedName("apple-itunes-app")
    val appleItunesApp: String,
    @SerializedName("format-detection")
    val formatDetection: String,
    @SerializedName("google:search-sre-monitor")
    val googleSearchSreMonitor: String,
    @SerializedName("og:description")
    val ogDescription: String,
    @SerializedName("og:image")
    val ogImage: String,
    @SerializedName("og:image:height")
    val ogImageHeight: String,
    @SerializedName("og:image:width")
    val ogImageWidth: String,
    @SerializedName("og:site_name")
    val ogSiteName: String,
    @SerializedName("og:title")
    val ogTitle: String,
    @SerializedName("og:type")
    val ogType: String,
    @SerializedName("og:url")
    val ogUrl: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("twitter:card")
    val twitterCard: String,
    @SerializedName("twitter:description")
    val twitterDescription: String,
    @SerializedName("twitter:image")
    val twitterImage: String,
    @SerializedName("twitter:image:src")
    val twitterImageSrc: String,
    @SerializedName("twitter:site")
    val twitterSite: String,
    @SerializedName("twitter:title")
    val twitterTitle: String,
    @SerializedName("viewport")
    val viewport: String
)