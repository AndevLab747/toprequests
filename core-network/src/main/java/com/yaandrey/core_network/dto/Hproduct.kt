package com.yaandrey.core_network.dto


import com.google.gson.annotations.SerializedName

data class Hproduct(
    @SerializedName("fn")
    val fn: String
)