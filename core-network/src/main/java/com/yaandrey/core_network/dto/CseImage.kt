package com.yaandrey.core_network.dto


import com.google.gson.annotations.SerializedName

data class CseImage(
    @SerializedName("src")
    val src: String
)