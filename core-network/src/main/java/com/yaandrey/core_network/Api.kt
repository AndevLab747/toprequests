package com.yaandrey.core_network

import com.yaandrey.core_network.dto.SearchResultDto
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("v1?key=AIzaSyBxiQKdL9VHwvoK_dwMmpCNOd-mllpYtPY&cx=011547213091933785851:krgmzgi4sfu")
    suspend fun getSearchResult(@Query("q") searchText: String): SearchResultDto
}