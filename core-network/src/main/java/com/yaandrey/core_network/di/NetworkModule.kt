package com.yaandrey.core_network.di

import com.example.core_usecase.repository.network.NetworkRepository
import com.yaandrey.core_network.Api
import com.yaandrey.core_network.BuildConfig
import com.yaandrey.core_network.NetworkRepositoryImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import org.koin.experimental.builder.single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://www.googleapis.com/customsearch/"

val networkModule = module {
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().apply {
                if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                        addInterceptor(this)
                    }
                }
            }.build())
            .build()
    }

    fun provideApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    single { provideRetrofit() }
    single { provideApi(get()) }

    single<NetworkRepositoryImpl>()
    factory<NetworkRepository> { get<NetworkRepositoryImpl>() }
}


