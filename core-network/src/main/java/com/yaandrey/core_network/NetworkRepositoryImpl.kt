package com.yaandrey.core_network

import com.example.core_usecase.model.SearchResultModel
import com.example.core_usecase.repository.network.NetworkRepository
import com.yaandrey.core_network.mapper.Mapper
import com.yaandrey.core_network.mapper.SearchResultMapper

class NetworkRepositoryImpl(private val api: Api) : NetworkRepository {

    override suspend fun loadSearchResult(searchText: String): List<SearchResultModel> {
        return try {
            val response = api.getSearchResult(searchText)
            SearchResultMapper.map(response)
        } catch (error: Throwable) {
            error.printStackTrace()
            emptyList()
        }
    }

}