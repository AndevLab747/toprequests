package com.yaandrey.core_storage.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "topResults")
data class SearchResultEntity(
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "url_web_site")
    val url: String,
    @ColumnInfo(name = "description_web_site")
    val description: String
)