package com.yaandrey.core_storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yaandrey.core_storage.entity.SearchResultEntity

@Dao
interface SearchResultDao : BaseDao<SearchResultEntity> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertResults(items: List<SearchResultEntity>)

    @Query("SELECT * FROM topResults order by id DESC limit 10")
    suspend fun getResult(): List<SearchResultEntity>?
}