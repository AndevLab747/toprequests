package com.yaandrey.core_storage.mapper

import com.example.core_usecase.model.SearchResultModel
import com.yaandrey.core_storage.entity.SearchResultEntity

object SearchResultMapper {

    fun toEntity(searchResultModelList: List<SearchResultModel>): List<SearchResultEntity> {
        val newArray = ArrayList<SearchResultEntity>()
        searchResultModelList.forEach {
            newArray.add(SearchResultEntity(0, it.title, it.url, it.description))
        }
        return newArray
    }

    fun fromEntity(searchResultEntityList: List<SearchResultEntity>?): List<SearchResultModel>? {
        val newArray = ArrayList<SearchResultModel>()
        searchResultEntityList?.forEach {
            newArray.add(SearchResultModel(it.title, it.url, it.description))
        }
        return newArray
    }
}