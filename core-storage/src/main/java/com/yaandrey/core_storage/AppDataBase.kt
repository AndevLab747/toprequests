package com.yaandrey.core_storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yaandrey.core_storage.dao.SearchResultDao
import com.yaandrey.core_storage.entity.SearchResultEntity

@Database(version = 3, exportSchema = false, entities = [SearchResultEntity::class])
abstract class AppDataBase : RoomDatabase() {
    abstract fun searchResultDao(): SearchResultDao
}