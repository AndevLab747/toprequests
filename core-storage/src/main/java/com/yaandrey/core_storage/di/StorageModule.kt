package com.yaandrey.core_storage.di

import android.app.Application
import androidx.room.Room
import com.example.core_usecase.repository.db.DataBaseRepository
import com.yaandrey.core_storage.AppDataBase
import com.yaandrey.core_storage.DataBaseRepositoryImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import org.koin.experimental.builder.single

val storageModule = module {

    fun provideAppDataBase(application: Application): AppDataBase =
        Room.databaseBuilder(application, AppDataBase::class.java, "topRequestsDB")
            .build()

    single { provideAppDataBase(androidApplication()) }
    single<DataBaseRepositoryImpl>()
    factory<DataBaseRepository> { get<DataBaseRepositoryImpl>()}
}