package com.yaandrey.core_storage

import com.example.core_usecase.model.SearchResultModel
import com.example.core_usecase.repository.db.DataBaseRepository
import com.yaandrey.core_storage.mapper.SearchResultMapper

class DataBaseRepositoryImpl(private val db: AppDataBase) : DataBaseRepository {

    override suspend fun insertResults(results: List<SearchResultModel>) {
        db.searchResultDao().insertResults(SearchResultMapper.toEntity(results))
    }

    override suspend fun getLastTenSearchResult(): List<SearchResultModel>? {
        return SearchResultMapper.fromEntity(db.searchResultDao().getResult())
    }
}