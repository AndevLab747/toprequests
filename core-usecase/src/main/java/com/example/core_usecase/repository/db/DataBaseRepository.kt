package com.example.core_usecase.repository.db

import com.example.core_usecase.model.SearchResultModel

interface DataBaseRepository {
    suspend fun insertResults(results: List<SearchResultModel>)
    suspend fun getLastTenSearchResult(): List<SearchResultModel>?
}