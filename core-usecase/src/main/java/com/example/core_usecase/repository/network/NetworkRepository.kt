package com.example.core_usecase.repository.network

import com.example.core_usecase.model.SearchResultModel

interface NetworkRepository{
    suspend fun loadSearchResult(searchText: String): List<SearchResultModel>
}