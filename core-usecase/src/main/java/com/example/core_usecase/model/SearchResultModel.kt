package com.example.core_usecase.model

data class SearchResultModel(
    val title: String,
    val url: String,
    val description: String
)